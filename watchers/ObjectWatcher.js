import Watcher from "./Watcher";
import ValueWatcher from "./ValueWatcher";
import {dioxide} from '../Dioxide';
import WatchingData from '../data/WatchingData';

export default class ObjectWatcher extends Watcher {
	constructor(data = dioxide.watchingData) { //data is container of type watching data for storing values of objects
		super(data);
		this.valueWatcher = new ValueWatcher();
	}

	watch(obj, callback) {		
		super.watch(obj);
		if (!callback) {
			throw 'You must pass callback parameter to watcher'
		}
		callback = callback.bind(obj);
		for (let key in obj) {
			this.valueWatcher.watch(obj, key, callback, [obj])
		}
	}

	off(obj) {
		for (let key in obj) {
			this.valueWatcher.off(obj, key)
		}
	}
}	
import Watcher from "./Watcher";
import {dioxide} from '../Dioxide';
import WatchingData from '../data/WatchingData';

export default class PointerWatcher extends Watcher {
	constructor(data = dioxide.watchingData) { //data is container of type watching data for contain values of objects
		super(data);
	}

	watch(obj, callback) {		
		super.watch(obj);	
		if (!callback) {
			throw 'You must pass callback parameter to watcher'
		}
		callback = callback.bind(obj);
		this.watchers[obj._dioxide.watchID] = this.watchers[obj._dioxide.watchID] || [];

		let uniqueKey = obj._dioxide.watchID + '_pointer';
		this.watchingData.push(uniqueKey, obj.value);

		let _watcher = setInterval(() => {
			let newValue = obj.value;
			let oldValue = this.watchingData.get(uniqueKey);
			if (newValue !== oldValue) {
				callback(newValue, oldValue, obj);
				this.watchingData.push(uniqueKey, obj.value);
			}
		}, 50);

		this.watchers[obj._dioxide.watchID].push(_watcher);
	}

	off(obj) {
		try {
			for (let _watcher of this.watchers[obj._dioxide.watchID]) {
				clearInterval(_watcher);
			}
			this.watchingData.del(obj._dioxide.watchID + '_pointer');
		} catch (e) {
			throw 'Object does not have watchID, you must start watching the key of this obj.';
		}
	}
}	
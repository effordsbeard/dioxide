import Watcher from "./Watcher";
import {dioxide} from '../Dioxide';
import WatchingData from '../data/WatchingData';

export default class ValueWatcher extends Watcher {
	constructor(data = dioxide.watchingData) {
		super(data);
	}

	watch(obj, key, callback, args = null) {
		super.watch(obj);
		if (!callback) {
			throw 'You must pass callback parameter to watcher'
		}
		callback = callback.bind(obj);
		if (!this.watchers[obj._dioxide.watchID]) this.watchers[obj._dioxide.watchID] = {};
		
		if(!this.watchers[obj._dioxide.watchID][key]) this.watchers[obj._dioxide.watchID][key] = [];

		let uniqueKey = key + obj._dioxide.watchID;
		this.watchingData.push(uniqueKey, obj[key]);

		let info = this.watchers[obj._dioxide.watchID][key];
		info.delay = 50;
		info.active = true;
		let self = this;
		info.lastChangeDate = new Date();
		function _watcher() {
			setTimeout(() => {
				let newValue = obj[key];

				let oldValue = self.watchingData.get(uniqueKey);	
				let now = new Date();
				switch(Math.round((now - info.lastChangeDate)/5000) ) {
					case 0 : info.delay = 70; break;
					case 1 : info.delay = 200; break;
					case 2 : info.delay = 300; break;
					case 3 : info.delay = 400; break;
					default : info.delay = 500;
				}
				if (newValue !== oldValue) {

					info.lastChangeDate = now;
					if (args) {
						callback.apply(obj, args);
					} else {
						callback(newValue, oldValue, obj);
					}
					self.watchingData.push(uniqueKey, obj[key]);
				}

				if (info.active) {
					_watcher();
				}
			}, info.delay);
		};

		info._watcher = _watcher;
		this.watchers[obj._dioxide.watchID][key].push(info);
		info._watcher();
	}

	off(obj, key) {
		try {
			for (let info of this.watchers[obj._dioxide.watchID][key]) {
				info.active = false;
			}
			this.watchingData.del(key + obj._dioxide.watchID);
		} catch (e) {
			throw 'Object does not have watchID, you must start watching the key of this obj.';
		}
	}
}	
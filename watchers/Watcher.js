export default class Watcher {
	constructor(data = dioxide.watchingData) { //data is container of type watching data for contain values of objects
		if (data === 'auto') {
			data = new WatchingData();
		}
		this.watchingData = data;
		this.watchers = {};
		this.watchID = 0; // needed to identify object again
	}

	watch(obj) {
		try {
			obj._dioxide.watchID
		} catch (e) {
			obj._dioxide = {};
		}
		if (!obj._dioxide.watchID) {
			obj._dioxide.watchID = this.watchID;
			this.watchID++;
		}
	}
}
import Request from '../network/Request';

export default class StaticLoader {
	constructor(url, callback) {
		this.req = new Request({
			method : 'GET',
			url : url
		}, (res) => {
			this.res = res;
			try {
				callback(res);
			} catch (e) {}
		});
	}

	load() {
		this.req.send();
	}
}
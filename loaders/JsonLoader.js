import StaticLoader from './StaticLoader';

export default class JsonLoader extends StaticLoader {
	constructor(url, callback) {
		super(url, function(res) {
			res.json = JSON.parse(res.body);
			callback(res);
		});
	}
}
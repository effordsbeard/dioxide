import StaticLoader from './StaticLoader';

export default class ImageLoader {
	constructor(url, callback = null, for_cache = true) {
		if (!for_cache && !callback) {
			throw 'if you load non static image you must pass callback'; 
		}

		this.url = url;
		this.image = new Image();
		try {
			this.image.onload = callback(this.image);
		} catch (e) {}
	}

	load() {
		this.image.src = this.url;
	}
}
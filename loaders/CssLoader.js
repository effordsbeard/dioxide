import StaticLoader from './StaticLoader';
import {dioxide} from '../Dioxide';

export default class CssLoader extends StaticLoader {
	constructor(url, callback) {
		super(url, function(res) {
			dioxide.style.innerText += res.body;
			callback(res);
		});
	}
}
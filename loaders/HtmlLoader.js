import StaticLoader from './StaticLoader';

export default class HtmlLoader extends StaticLoader {
	constructor(url, callback, el) {
		super(url, function(res) {
			try {
				el.innerHTML = res.body;
			} catch (e) {

			}	
			try {
				callback(res);
			} catch (e) {

			}	
		});
	}
}
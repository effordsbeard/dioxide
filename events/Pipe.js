export default class Pipe {
	constructor() {
		this.events = {};
	}

	send(msg, ...args) {
		if (!(msg in this.events))
			return;
		for (let func of this.events[msg]) {
			func.apply(null, args);
		}
		return this;
	}

	on(msg, func) {
		if (!(msg in this.events)) {
			this.events[msg] = [];
		}
		this.events[msg].push(func);
	}

}
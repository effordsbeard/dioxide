export default class Pointer {
	constructor(value) {
		this.value = value;
	}

	valueOf() {
		return this.value.valueOf();
	}

	toString() {
		return this.value.toString();
	}
}
import WatchingData from './data/WatchingData';

class Dioxide {
	constructor() {
		this.watchingData = new WatchingData();
		this.config = {
			perfomance : 'normal'
		};
	}

	// methods
	init () {
		this.style = document.createElement('style'); //just container for css will be loaded
		this.style.type = 'text/css';
		document.getElementsByTagName('head')[0].appendChild(this.style);
	}
}

export var dioxide = new Dioxide();
import ValueWatcher from '../watchers/ValueWatcher';

export default class Router  {
	constructor(routes) {
		this.routes = {};
		for( var route in routes ){
			var methodName = routes[route];
			this.routes.push({
				pattern: new RegExp('^'+route.replace(/:\w+/, '(\\w+)')+'$'),
				callback: this[methodName]
			});
		}
		this.path = window.location.pathname;
		this.routes[this.path]();
		
		let watcher = new ValueWatcher();
		watcher.watch(window.location, 'pathname', (_new) => {
			this.path = _new;
			try {
				this.routes[_new]();
			} catch(e) {
				console.log('not all routers can handle this path')
			}
		});
	}

	getRouteFunction() {
		return this.routes[this.path];
	}

	push(route) {
		window.history.pushState(null, null, route);
	}

	forward() {
		window.history.forward()
	}
	
	back() {
		window.history.back()
	}
}
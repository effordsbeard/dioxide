import ValueWatcher from '../watchers/ValueWatcher';
import Template from '../templating/Template';
import Raw from '../templating/Raw';

export default function dx(tagName, ...args) {
	try {
		args[0].context[args[0].key]; // if second parameter is connection for binding
		return new BindedDX(tagName, args);
	} catch (e) {
		return new ComplexDX(tagName, args);
	}
}

class DX {
	constructor(tagName = 'div', args = []) {
		this.tagName = tagName;
		this.args = args;
		this.el = document.createElement(tagName);
	}

	a(attr, value) { //setting attribute
		try {
			let _value = value.context[value.key]; //if value is connection
			this.el.setAttribute(attr, _value);
			let attrWatcher = new ValueWatcher();
			attrWatcher.watch(value.context, value.key, (_new, _old) => {
				this.el.setAttribute(attr, _new);
			});
		} catch (e) {
			this.el.setAttribute(attr, value);
		}
		return this;
	}

	on(event, callback) {
		this.el.addEventListener(event, (e) => {
			callback(e);
		});
		return this;
	}

	appendTo(parent) {
		parent.appendChild(this.el);
	}

}

let contentWatcher = new ValueWatcher();

class BindedDX extends DX {
	constructor(tagName, args = []) {
		super(tagName, args);
		let connection = args[0];
		this.el.innerText = connection.context[connection.key];
		contentWatcher.watch(connection.context, connection.key, (_new, _old) => {
			this.el.innerText = _new;
		});
	}

	// methods
}

class ComplexDX extends DX {
	constructor(tagName, args = []) {
		super(tagName, args);
		for (let elem of args) {
			//elem can be Array of Template, DX, Number, String instance or just only instance
			let elems = [].concat(elem);
			for (let _elem of elems) {
				// if arg is DOM component
				if (_elem instanceof Element) {
					this.el.appendChild(_elem);
					continue;
				}

				// if another template
				if (_elem instanceof Template) {
					this.el.appendChild(_elem.root)
					continue;
				}

				// if DX
				if (_elem instanceof DX) {
					this.el.appendChild(_elem.el);
					continue;
				}

				// if raw html
				if (_elem instanceof Raw) {
					let div = document.createElement('div');
					div.innerHTML = _elem.html;
					for (var i = 0; i < div.childNodes.length; i++) {
						this.el.appendChild(div.childNodes[i]);
					}
					continue;
				}

				// if Number or String
				try {
					let node = document.createTextNode(_elem.toString());
					this.el.appendChild(node);
				} catch (e) {
					console.log('dioxide warning: you have undefined or null element inside DX')
				}

			}

		}
	}
}
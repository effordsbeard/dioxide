import Data from './Data';

export default class WatchingData extends Data {
	constructor() {
		super();
		this.data = {};
	}

	push(key, value) {
		this.data[key] = value;
	}

	get (key) {
		return this.data[key];
	}
	
	del(key) {
		delete this.data[key];
	}

	clear() {
		this.data = {};
	}
}
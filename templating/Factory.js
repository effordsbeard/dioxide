import Template from "./Template";

export default class Factory {
	constructor(templateName, TemplateClass, params) {
		if (!params.props) {
			params.props = {}
		}
		this.templatedElements = [];

		let _elements = document.querySelectorAll(`*[data-dx-template]`)
		let elements = [];
		for (let i = 0; i < _elements.length; i++) {
			let element = _elements[i];
			if (element.getAttribute('data-dx-template') === templateName) {
				elements.push(element);
			}
		}
		for (let el of elements) {
			let _params = JSON.parse(JSON.stringify(params));
			_params.el = el;
			for (let i = 0; i < el.attributes.length; i++) {
				let attr = el.attributes[i];
				let name = attr.nodeName;
				let value = attr.nodeValue;
				if (!name.startsWith('data-dx-prop')) continue;
				_params.props[name.split('-').slice(3).join('-')] = value;
			}
			_params.compiled = params.compiled;
			_params.ready = params.ready;
			let templated = new TemplateClass(_params);
			this.templatedElements.push(templated);
		}
	}

	map(mapFunc) {
		this.templatedElements.map(mapFunc);
	}
}
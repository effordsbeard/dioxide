import HtmlLoader from '../loaders/HtmlLoader';
import ValueWatcher from '../watchers/ValueWatcher';
import dx from '../virtual/dom';

export default class Template {
	constructor(root = document.createElement('div')) {
		this.root = root;
		this.parent = document.body;
	}

	render(parent = this.parent) {
		this.parent = parent;
		this.compile();
		try {
			this.parent.parentNode.replaceChild(this.root, this.parent);
		} catch (e) {}
	}

	compile(newDX) {
		this._DX = newDX;
		
		if (newDX) {
			this.parent.parentNode.replaceChild(newDX.el, this.parent);
		}
		return this;
	}

	addClass(target, className) {
		this.setClassnamesSet(target);
 		this['_dxclass__' + target].add(className);
		this.updateClassname(target);
		return this;
	}

	removeClass(target, className) {
		this.setClassnamesSet(target);
		this['_dxclass__' + target].delete(className);
		this.updateClassname(target);
		return this;
	}

	hasClass(target, className) {
		this.setClassnamesSet(target);
		return this['_dxclass__' + target].has(className);
	}

	toggleClass(target, className) {
		this.setClassnamesSet(target);
		if (this.hasClass(target, className)) {
			this.removeClass(target, className)
		} else {
			this.addClass(target, className)
		}
		return this;
	}


	toggleClasses(target, class_1, class_2) {
		this.setClassnamesSet(target);
		if (this.hasClass(target, class_1)) {
			this.removeClass(target, class_1).addClass(target, class_2);
		} else {
			this.removeClass(target, class_2).addClass(target, class_1);
		}
		return this;
	}

	updateClassname(target) { // private
		this[target] = [...this['_dxclass__' + target]].join(' ');
	}

	setClassnamesSet(target) {
		// if no Set() of target classNames set
		if (!this['_dxclass__' + target]) {
			this['_dxclass__' + target] = new Set(this[target].split(/ +/g));
		}
	}
	// method for binding values in dx()
	connect(key) { 
		return {
			context : this,
			key : key
		}
	}
}	
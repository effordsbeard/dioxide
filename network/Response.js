export default class Response {
	constructor(status, statusText, responseText, headers) {
		this.status = status;
		this.statusText = statusText;
		this.body = responseText;
		this.headers = {};

		let headerLines = headers.split('\r\n');
		for (let header of headerLines) {
			let [name, value] = header.split(': ');
			this.headers[name] = value; 
		}
	}

	header(name) {
		return this.headers[name];
	}
}	
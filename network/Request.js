import Response from './Response';

export default class Request {
	constructor(params, callback) {
		this.xhr = new XMLHttpRequest();
		this.async = true;
		this.method = params.method;
		this.url = params.url;
		this.headers = params.headers;
		this.body = params.body;
		let self = this;
		this.xhr.onload = function() {
			self.response = new Response(self.xhr.status, self.xhr.statusText, self.xhr.responseText, self.xhr.getAllResponseHeaders());
			try {
				callback(self.response)
			} catch (e) {
				
			}
 		}

	}
	send() {
		if (this.headers && this.headers['Content-Type'] === 'application/json') {
			try {
				this.body = JSON.stringify(this.body);
			} catch (e) {
				throw 'DIOXIDE: cannot parse JSON body for ajax request';
			}
		}
		try {
			this.user = params.auth.user;
			this.password = params.auth.password;
			this.xhr.open(this.method, this.url, this.async, this.user, this.password)
		} catch (e) {
			this.xhr.open(this.method, this.url, this.async)
		}
		for (let header in this.headers) {
			this.xhr.setRequestHeader(header, this.headers[header]);
		}
		if (this.method !== 'GET') {
			this.xhr.send(this.body);
		} else {
			this.xhr.send();
		}
	}

	stop() {
		this.xhr.abort()
	}

	makeSync() {
		this.params.async = true;
		return this;
	}
	makeAsync() {
		this.params.async = false;
		return this;
	}
	// methods
}